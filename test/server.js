const expect  = require("chai").expect;
const request = require("request");

describe("echo name request", function() {
    const url = "http://localhost:3000/echo?name=ruzica";

    it("assert 200 OK", function(done) {
        request(url, function(error, response, body) {
            expect(response.statusCode).to.equal(200);
            done();
        });
    });

    it("assert name", function(done) {
        request(url, function(error, response, body) {
            expect(body).to.equal("Hello ruzica");
            done();
        });
    });
});
