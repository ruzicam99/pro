const expect = require("chai").expect;
const echo = require("../app/echo");

describe("echo username", function () {
        it("echoes username", function () {
            const ehlo = echo.echo("Ruzica");
            expect(ehlo).to.equal("Hello Ruzica");
        });
});
