FROM node:12-alpine as builder

WORKDIR /usr/src/app

COPY package.json ./
RUN npm update
COPY . .
RUN node app/server & sleep 10 & npm test

FROM node:12-alpine

WORKDIR /usr/src/app

COPY --from=builder /usr/src/app/node_modules ./node_modules
COPY --from=builder /usr/src/app/app ./app

ENV PATH /usr/src/app/node_modules/.bin:$PATH
RUN chown -R node:node /usr/src/app
USER node
WORKDIR /usr/src/app
CMD [ "node", "./app/server" ]
