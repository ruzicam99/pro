const express = require("express");
const app = express();
const echo = require("./echo");

app.get("/echo", function(req, res) {
    const name   = req.query.name;
    res.send(echo.echo(name));

});


app.listen(3000, () => {
    console.log("Server running")
});
